from dotenv import load_dotenv
import os
import streamlit as st
from groq import Groq

load_dotenv('.env')

def get_groq_completions(user_content):
    client = Groq(
        api_key=os.environ.get("GROQ_API_KEY"),
    )

    completion = client.chat.completions.create(
        model="mixtral-8x7b-32768",
        messages=[
            {                
                "role": "system",
                "content": "Du führst eine Unterhaltung auf Deutsch und antwortest immer in dieser Sprache, ohne dass es extra erwähnt werden muss. Du bist ein kenntnisreicher und leidenschaftlicher Fußballexperte mit einem entspannten und freundlichen Kommunikationsstil. Deine Aufgabe ist es, detaillierte und interessante Informationen über Fußballspieler zu liefern. Beginne mit einer kurzen Einführung zum Spieler, gefolgt von einer strukturierten Auflistung wichtiger Daten: \n\n- Name: \n- Geburtsdatum und Ort: \n- Aktueller Verein oder bei Karriereende das Enddatum: \n- Ehemalige Vereine: \n- Nationalität: \n- Nationalmannschaftseinsätze: \n- Größte Erfolge: \n- Individuelle Auszeichnungen: \n\nBitte berücksichtige nur echte Fußballspieler. Sollte der genannte Name nicht einem Fußballspieler entsprechen, bitte den Nutzer um die Nennung eines Fußballers mit den Worten: 'Bitte nenne mir einen Fußballer.'"
                
            },
            {
                "role": "user",
                "content": user_content
            }
        ],
        temperature=0.5,
        max_tokens=3000,
        top_p=1,
        stream=True,
        stop=None,
    )

    result = ""
    for chunk in completion:
        result += chunk.choices[0].delta.content or ""
    return result

def main():
    st.title("Fußballer-Generator")
    user_content = st.text_input("Gebe hier einen Fußballer-Namen ein:", on_change=None, key="input")

    if user_content:
        # Anzeigen einer Nachricht während der Generierung der Inhalte
        with st.spinner("Ich suche..."):
            generated_content = get_groq_completions(user_content)
            # st.markdown(f"### {user_content}:") # Überschrift
            st.text_area("", value=generated_content, height=800)

if __name__ == "__main__":
    main()
