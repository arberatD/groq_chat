provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key_id
  secret_key = var.aws_secret_access_key
}

resource "aws_key_pair" "soccer_app_key" {
  key_name   = "soccer_app_key"
  public_key = file("${path.module}/soccer_app_key.pem.pub")
}

resource "aws_instance" "soccer_app" {
  ami                    = "ami-01be94ae58414ab2e"
  instance_type          = "t2.micro"
  subnet_id              = "subnet-0231beeeb944c0d68"
  key_name               = aws_key_pair.soccer_app_key.key_name
  vpc_security_group_ids = ["sg-09624af50b82b1808"]

  tags = {
    Name = "SoccerAppInstance"
  }
}

resource "aws_s3_bucket" "soccer_bucket" {
  bucket = "soccerbucketgroq"

  tags = {
    Name = "SoccerBucketGroq"
  }
}

output "public_ip" {
  description = "Die öffentliche IP-Adresse der EC2-Instanz"
  value       = aws_instance.soccer_app.public_ip
}

output "security_group_id" {
  description = "Die ID der Sicherheitsgruppe, die mit der EC2-Instanz verbunden ist"
  value       = "sg-09624af50b82b1808"
}

output "s3_bucket_name" {
  description = "Der Name des S3-Buckets"
  value       = aws_s3_bucket.soccer_bucket.bucket
}
